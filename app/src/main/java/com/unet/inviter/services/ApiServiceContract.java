package com.unet.inviter.services;

public final class ApiServiceContract {

    public static final String BROADCAST_INVITE_STATUS =
            "com.unet.inviter.services.ApiServiceContract.BROADCAST_INVITE_STATUS";

    public static final String EXTRA_STATUS =
            "com.unet.inviter.services.ApiServiceContract.EXTRA_STATUS";
    public static final String EXTRA_ERROR =
            "com.unet.inviter.services.ApiServiceContract.EXTRA_ERROR";
    public static final String EXTRA_ERROR_MESSAGE =
            "com.unet.inviter.services.ApiServiceContract.EXTRA_ERROR_MESSAGE";
    public static final String EXTRA_DATA =
            "com.unet.inviter.services.ApiServiceContract.EXTRA_DATA";

    public enum Status {
        DONE, ERROR
    }

    public enum Error {
        AUTH_ERROR, // Authentication error
        NETWORK_ERROR, // Network related error
        SERVER_ERROR, // Server side error
        REQUEST_ERROR, // Wrong request - see EXTRA_TASK_ERROR_MESSAGE
        TASK_ERROR // Exception in service code - see EXTRA_TASK_ERROR_MESSAGE
    }

}
