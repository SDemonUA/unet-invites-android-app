package com.unet.inviter;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Response;

/**
 * This class designed to unify responses from AsyncTasks that perform network requests
 *
 * @param <T> type of successful response data
 */
public class AsyncTaskResult<T> {
    private T result;
    private String error;
    private String errorDescription;
    private int statusCode;
    private boolean hasError;

    public AsyncTaskResult(T result) {
        this.result = result;
        this.statusCode = 200;
        hasError = false;
    }

    public AsyncTaskResult(String error, String errorDescription) {
        this.statusCode = 400;
        this.error = error;
        this.errorDescription = errorDescription;
        hasError = true;
    }

    public AsyncTaskResult(int statusCode, String error, String errorDescription) {
        this(error, errorDescription);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    interface ResultExtractor<T>{
        T extract(JSONObject responseData);
    }
    public AsyncTaskResult(Response resp, ResultExtractor<T> extractor){
        JSONObject respData = new JSONObject();
        try {
            respData.put("error", "Error");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (resp.body() != null){
            try {
                respData = new JSONObject(resp.body().string());
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if (!resp.isSuccessful() || respData.has("error")){
            this.statusCode = resp.code();
            this.error = respData.optString("error", resp.message());
            this.errorDescription = respData.optString("error_description");
            hasError=true;
            return;
        }

        try {
            this.result = extractor.extract(respData);
            hasError=false;
        } catch (Exception e){
            this.error = e.getMessage();
            hasError=true;
        }
    }

    @Nullable
    public T getResult() {
        return result;
    }

    @Nullable
    public String getError() {
        return error;
    }

    @Nullable
    public String getErrorDescription() {
        return errorDescription;
    }

    public boolean hasError() {
        return hasError;
    }


}
