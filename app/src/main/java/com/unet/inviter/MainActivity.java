package com.unet.inviter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.unet.inviter.db.AppDao;
import com.unet.inviter.db.AppDatabaseFactory;
import com.unet.inviter.db.Invite;
import com.unet.inviter.db.Site;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class MainActivity
        extends AppCompatActivity
        implements ContactsCursorRecyclerViewAdapter.OnItemClickListener,
        LoaderManager.LoaderCallbacks<Cursor>,
        SitesListFragment.OnListFragmentInteractionListener {

    private static final int REQUEST_READ_CONTACTS_PERMISSION = 100;

    // The column index for the _ID column
    private static final int CONTACT_ID_INDEX = 0;
    // The column index for the LOOKUP_KEY column
    private static final int CONTACT_KEY_INDEX = 1;
    private static final int CONTACT_NAME_INDEX = 2;

    private static final String[] CONTACTS_PROJECTION =
            {
                    ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.LOOKUP_KEY,
                    ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
            };

    private String mSearchString = "";

    // An adapter that binds the result Cursor to the ListView
    private ContactsCursorRecyclerViewAdapter mContactsCursorAdapter;

    private long mSelectedCommunity = -1;
    private boolean mInviteMessageEditorShown = false;
    private String mSelectedCommunityTitle;

    private Set<Long> mExcludedContacts = new HashSet<>();
    private long previousBackPressToQuit = 0;

    long getSelectedCommunityId() {
        return mSelectedCommunity;
    }

    @SuppressLint("deprecation")
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        restoreState();

        // TODO: 07.09.2018 Replace defined in xml fragment with dynamically inserted one to pass arguments to it

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // AppBarLayout appBarLayout = findViewById(R.id.appbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toggle.getDrawerArrowDrawable().setColor(getColor(R.color.iconColor));
        }
        else {
            toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.iconColor));
        }

        setupNavigationHeader(findViewById(R.id.nav_header));

        initInviteMessageEditor();

        setupContactList();
        retrieveContacts();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mSelectedCommunity == -1) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer != null) {
                drawer.openDrawer(GravityCompat.START);
            }
        }
    }

    private void restoreState() {

        SharedPreferences opts = getSharedPreferences("opts", MODE_PRIVATE);
        mSelectedCommunity = opts.getLong("selected_community_id", -1);
        mSelectedCommunityTitle = opts.getString("selected_community_title", null);

        if (mSelectedCommunity != -1) {
            setTitle(mSelectedCommunityTitle);

            initExcludedContacts(mSelectedCommunity, opts);

            String messagePreset = opts.getString(
                    "inv_message_preset_" + mSelectedCommunity,
                    getResources().getString(R.string.sample_invite_msg).replace("$community_name", mSelectedCommunityTitle)
            );

            View view = findViewById(R.id.invite_msg_editor);
            EditText textView = view.findViewById(R.id.text);

            textView.setText(messagePreset);
        }
    }

    private void initExcludedContacts(long communityId) {
        SharedPreferences opts = getSharedPreferences("opts", MODE_PRIVATE);
        initExcludedContacts(communityId, opts);
    }

    private void initExcludedContacts(long communityId, SharedPreferences opts) {
        // Read from opts and update cursor
        String exclContacts = opts.getString("excluded_contacts" + communityId, "");
        Set<Long> excludedContacts = new HashSet<>();
        for (String id : exclContacts.split(",")) {
            if (id.isEmpty()) continue;
            excludedContacts.add(Long.valueOf(id));
        }

        mExcludedContacts = excludedContacts;
    }

    private void excludeContact(long communityId, long contactId) {
        if (mSelectedCommunity == communityId) {
            if (mExcludedContacts.add(contactId)) {
                saveExcludedContacts(communityId);
                getSupportLoaderManager().restartLoader(0, null, this);
            }
        }
    }

    private void restoreContact(long communityId, long contactId) {
        if (mSelectedCommunity == communityId) {
            if (mExcludedContacts.remove(contactId)) {
                mExcludedContacts.remove(contactId);
                saveExcludedContacts(communityId);
                getSupportLoaderManager().restartLoader(0, null, this);
            }
        }
    }

    private void saveExcludedContacts(long communityId) {
        if (mSelectedCommunity != communityId) {
            return;
        }

        StringBuilder sb = new StringBuilder();
        for (long contactId : mExcludedContacts) {
            if (sb.length() > 0) sb.append(",");
            sb.append(contactId);
        }

        SharedPreferences opts = getSharedPreferences("opts", MODE_PRIVATE);
        opts.edit().putString("excluded_contacts" + communityId, sb.toString()).apply();
    }

    public void onLoadContactsBtnClick(View v) {
        retrieveContacts();
    }

    private void retrieveContacts() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS_PERMISSION);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        } else {
            // Permission has already been granted

            LoaderManager lm = getSupportLoaderManager();
            if (lm.getLoader(0) == null) {
                lm.initLoader(0, null, this);
            } else {
                lm.restartLoader(0, null, this);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CONTACTS_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    findViewById(R.id.contact_list).setVisibility(View.VISIBLE);
                    findViewById(R.id.empty_contact_list).setVisibility(View.VISIBLE);
                    findViewById(R.id.load_contacts_btn).setVisibility(View.GONE);

                    retrieveContacts();
                } else {
                    // Permission denied
                    findViewById(R.id.contact_list).setVisibility(View.GONE);
                    findViewById(R.id.empty_contact_list).setVisibility(View.GONE);
                    findViewById(R.id.load_contacts_btn).setVisibility(View.VISIBLE);
                }
                break;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void setupContactList() {
        final RecyclerView contactsView = findViewById(R.id.contact_list);
        final TextView emptyView = findViewById(R.id.empty_contact_list);

        EditText searchField = findViewById(R.id.search_field);
        if (searchField != null) {
            searchField.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    mSearchString = editable.toString();
                    getSupportLoaderManager().restartLoader(0, null, MainActivity.this);
                }
            });
        }


        contactsView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        ContactsCursorRecyclerViewAdapter.CustomRecycleViewTouchHelper touchHelper = new ContactsCursorRecyclerViewAdapter.CustomRecycleViewTouchHelper(contactsView);
        new ItemTouchHelper(touchHelper).attachToRecyclerView(contactsView);

        mContactsCursorAdapter = new ContactsCursorRecyclerViewAdapter(null, CONTACT_ID_INDEX, CONTACT_NAME_INDEX, null, this, this);
        contactsView.setAdapter(mContactsCursorAdapter);

        mContactsCursorAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if (mContactsCursorAdapter.getItemCount() == 0) {
                    emptyView.setVisibility(View.VISIBLE);
//                    contactsView.setVisibility(View.GONE);
                } else {
                    emptyView.setVisibility(View.GONE);
//                    contactsView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setupNavigationHeader(View headerView) {
        if (headerView == null) return;

        SharedPreferences userInfo = getSharedPreferences("user_info", MODE_PRIVATE);

        String fullName = String.format("%s %s", userInfo.getString("last_name", ""), userInfo.getString("first_name", ""));

        ((TextView) headerView.findViewById(R.id.name)).setText(fullName);
//        ((TextView) headerView.findViewById(R.id.profile_url)).setText(userInfo.getString("profile_url", ""));

        String photoUrl = userInfo.getString("photo_url", null);
        if (photoUrl != null) {
            Picasso.get()
                    .load(photoUrl)
                    .into((ImageView) headerView.findViewById(R.id.photo));
        }

        headerView.findViewById(R.id.leave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AuthStateManager.getInstance(MainActivity.this).replace(new AuthState());
                getSharedPreferences("user_info", MODE_PRIVATE).edit().clear().apply();

                getSharedPreferences("opts", MODE_PRIVATE).edit()
                        .remove("selected_community_id")
                        .remove("selected_community_title")
                        .apply();

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (mInviteMessageEditorShown) {
            hideInviteMessageEditor();
        } else {
            // Double back press to quit support
            long currentTime = System.currentTimeMillis();
            if (currentTime - previousBackPressToQuit < 2000) {
                finish();
                // finishAndRemoveTask();
                // super.onBackPressed(); // Exit
            } else {
                previousBackPressToQuit = currentTime;
                Toast.makeText(this, R.string.back_again_to_quit, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_edit_invite_msg);
        if (item != null) item.setVisible(!mInviteMessageEditorShown);

        item = menu.findItem(R.id.action_edit_invite_msg_cancel);
        if (item != null) item.setVisible(mInviteMessageEditorShown);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_edit_invite_msg:
                showInviteMessageEditor();
                break;
            case R.id.action_edit_invite_msg_cancel:
                hideInviteMessageEditor();

                // Reset text
                SharedPreferences opts = getSharedPreferences("opts", MODE_PRIVATE);
                String messagePreset = opts.getString("inv_message_preset_" + mSelectedCommunity, getResources().getString(R.string.sample_invite_msg).replace("$community_name", mSelectedCommunityTitle));

                View view = findViewById(R.id.invite_msg_editor);
                EditText textView = view.findViewById(R.id.text);

                textView.setText(messagePreset);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void initInviteMessageEditor() {
        View view = findViewById(R.id.invite_msg_editor);
        final EditText textView = view.findViewById(R.id.text);
        Button saveBtn = view.findViewById(R.id.btn_save);

        final Button nameButton = view.findViewById(R.id.name_btn);
        final Button linkButton = view.findViewById(R.id.link_btn);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = v.getId() == R.id.name_btn ? "$name" : "$link";
                int start = Math.max(textView.getSelectionStart(), 0);
                int end = Math.max(textView.getSelectionEnd(), 0);
                textView.getText().replace(start, end, text);
            }
        };
        nameButton.setOnClickListener(clickListener);
        linkButton.setOnClickListener(clickListener);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable spannable) {
                String message = spannable.toString();
                StyleSpan[] spans = spannable.getSpans(0, message.length(), StyleSpan.class);
                for (StyleSpan span : spans) {
                    spannable.removeSpan(span);
                }

                for (String tmpl : new String[]{"$name", "$link"}) {
                    int start, end = 0;
                    boolean present = false;

                    while (message.indexOf(tmpl, end) != -1) {
                        start = message.indexOf(tmpl, end);
                        end = start + tmpl.length();

                        spannable.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        present = true;
                    }

                    (tmpl.equals("$name") ? nameButton : linkButton).setEnabled(!present);
                }

            }
        };
        textView.addTextChangedListener(textWatcher);
        textWatcher.afterTextChanged(textView.getText()); // Trigger on already inserted text
//        textView.setText(messagePreset);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View containerView = findViewById(R.id.invite_msg_editor);
                EditText textView = containerView.findViewById(R.id.text);
                String text = textView.getText().toString();
                if (text.isEmpty()) {
                    text = getResources().getString(R.string.sample_invite_msg).replace("$community_name", mSelectedCommunityTitle);
                }

                SharedPreferences opts = getSharedPreferences("opts", MODE_PRIVATE);
                SharedPreferences.Editor edit = opts.edit();
                edit.putString("inv_message_preset_" + mSelectedCommunity, text);
                edit.apply();

                hideInviteMessageEditor();
            }
        });
    }

    private void showInviteMessageEditor() {
        if (mInviteMessageEditorShown) return;
        if (mSelectedCommunity == -1) {
            askToSelectCommunity();
            return;
        }

        View view = findViewById(R.id.invite_msg_editor);
        view.setVisibility(View.VISIBLE);

        view.setAlpha(0);
        view.animate().alpha(1).setDuration(200);

        View animationView = view.findViewById(R.id.content);
        animationView.setTranslationY(-50);
        animationView.animate()
                .translationY(0)
                .setDuration(200);

        mInviteMessageEditorShown = true;

        invalidateOptionsMenu();
    }

    private void hideInviteMessageEditor() {
        if (!mInviteMessageEditorShown) return;

        final View view = findViewById(R.id.invite_msg_editor);
        View animationView = view.findViewById(R.id.content);

        view.setAlpha(1);
        view.animate().alpha(0).setDuration(200);

        animationView.setTranslationY(0);
        animationView.animate()
                .translationY(-50)
                .setDuration(200)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        view.setVisibility(View.GONE);
                        mInviteMessageEditorShown = false;

                        invalidateOptionsMenu();
                    }
                });
    }

    private void askToSelectCommunity(){
        Toast toast = Toast.makeText(this, R.string.select_site_warn, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void onItemClick(final Cursor cursor, View view, int position) {
        if (cursor == null) return;
        if (mSelectedCommunity == -1) {
            askToSelectCommunity();
            return;
        }

        // Move to the selected contact
        cursor.moveToPosition(position);

        // Get the _ID value
        final long id = cursor.getLong(CONTACT_ID_INDEX);


        if (view.getId() == R.id.ignoreBtn){
            excludeContact(mSelectedCommunity, id);
            return;
        }

        pickShortContactName(id, new OnNamePicked() {
            @Override
            public void onName(String name) {
                // Get the selected LOOKUP KEY
                String contactKey = cursor.getString(CONTACT_KEY_INDEX);

                Intent intent = new Intent(MainActivity.this, MessageActivity.class);
                intent.putExtra(MessageActivity.EXTRA_CONTACT_ID, id);
                intent.putExtra(MessageActivity.EXTRA_CONTACT_KEY, contactKey);
                intent.putExtra(MessageActivity.EXTRA_COMMUNITY_ID, mSelectedCommunity);
                intent.putExtra(MessageActivity.EXTRA_COMMUNITY_TITLE, mSelectedCommunityTitle);
                intent.putExtra(MessageActivity.EXTRA_CONTACT_NAME, name);
                intent.putExtra(MessageActivity.EXTRA_CONTACT_DISPLAY_NAME, cursor.getString(CONTACT_NAME_INDEX));

                startActivity(intent);
            }
        });


    }

    interface OnNamePicked{
        void onName(String name);
    }
    void pickShortContactName(final long contactId, @NonNull final OnNamePicked callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ContentResolver cr = getContentResolver();

                Cursor cursor = cr.query(
                        ContactsContract.Data.CONTENT_URI,
                        new String[]{
                                ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                                ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                                ContactsContract.CommonDataKinds.StructuredName.IS_SUPER_PRIMARY,
                        },
                        ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.CONTACT_ID + " = ?",
                        new String[]{
                                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
                                String.valueOf(contactId)
                        },
                        null
                );

                if (cursor == null){
                    callback.onName(null);
                    return;
                }

                int GIVEN_NAME_IDX = 0;
                int DISPLAY_NAME_IDX = 1;
                int IS_PRIMARY_IDX = 2;

                String givenName = null;
                String displayName = null;
                while( cursor.moveToNext() ){
                    if ( givenName == null || cursor.getInt(IS_PRIMARY_IDX) > 0){
                        if (cursor.getString(GIVEN_NAME_IDX) != null)
                            givenName = cursor.getString(GIVEN_NAME_IDX);
                    }

                    if ( displayName == null || cursor.getInt(IS_PRIMARY_IDX) > 0){
                        if (cursor.getString(DISPLAY_NAME_IDX) != null)
                            displayName = cursor.getString(DISPLAY_NAME_IDX);
                    }
                }

                cursor.close();

                final String resultName = givenName != null ? givenName : displayName;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onName(resultName);
                    }
                });

            }
        }).start();
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        String selectionString =
                ContactsContract.Contacts.HAS_PHONE_NUMBER + " = ?";
        String[] selectionArgs = {"1"};

        if (mExcludedContacts.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (long contactId : mExcludedContacts) {
                if (sb.length() > 0) sb.append(",");
                sb.append(contactId);
            }
            selectionString += " AND " + ContactsContract.Contacts._ID + " NOT IN(" + sb.toString() + ")";
        }

        if (!mSearchString.isEmpty()) { // Search by name
            selectionString += " AND " + ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " LIKE ?";
            selectionArgs = Arrays.copyOf(selectionArgs, selectionArgs.length + 1);
            selectionArgs[selectionArgs.length - 1] = "%" + mSearchString + "%";
        }

        return new CursorLoader(
                this,
                ContactsContract.Contacts.CONTENT_URI,
                CONTACTS_PROJECTION,
                selectionString,
                selectionArgs,
                ContactsContract.Contacts.SORT_KEY_PRIMARY
        );
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        // Put the result Cursor in the adapter for the ListView
        AppDao dao = AppDatabaseFactory.getDatabase(this).appDao();
        mContactsCursorAdapter.swapCursorAndInvites(data, dao.getInvitesBySite(mSelectedCommunity));
        mContactsCursorAdapter.setHighlight(mSearchString);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        // Delete the reference to the existing Cursor
        mContactsCursorAdapter.swapCursor(null);
    }


    @Override
    public void onListFragmentInteraction(Site item) {

        if (item != null) {
            setTitle(item.getTitle());
            mSelectedCommunityTitle = item.getTitle();
            mSelectedCommunity = item.getId();
            initExcludedContacts(mSelectedCommunity);

            SharedPreferences opts = getSharedPreferences("opts", MODE_PRIVATE);
            String messagePreset = opts.getString(
                    "inv_message_preset_" + mSelectedCommunity,
                    getResources().getString(R.string.sample_invite_msg).replace("$community_name", mSelectedCommunityTitle)
            );

            View view = findViewById(R.id.invite_msg_editor);
            EditText textView = view.findViewById(R.id.text);

            textView.setText(messagePreset);


            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        } else {
            setTitle(R.string.title_activity_main);
            mSelectedCommunityTitle = null;
            mSelectedCommunity = -1;
        }

        SharedPreferences opts = getSharedPreferences("opts", MODE_PRIVATE);
        opts.edit()
                .putString("selected_community_title", mSelectedCommunityTitle)
                .putLong("selected_community_id", mSelectedCommunity)
                .apply();

        retrieveContacts();
    }



    private void fetchInvites(final Site site) {
        AuthState authState = AuthStateManager.getInstance(this).getCurrent();
        AuthorizationService authService = new AuthorizationService(this);
        authState.performActionWithFreshTokens(authService, new AuthState.AuthStateAction() {
            @Override
            public void execute(@Nullable String accessToken, @Nullable String idToken, @Nullable AuthorizationException ex) {
                if(ex != null){
                    Utils.handleAuthError(MainActivity.this, findViewById(android.R.id.content), ex);
                    return;
                }

                AuthStateManager.getInstance(MainActivity.this).update();
                AsyncTaskResult<ArrayList<Invite>> res = null;

                try {
                    res = new GetInvitesTask().execute(new GetInvitesTask.ParamsTuple(accessToken, site.getResourceUri(), site.getId())).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                if (res == null || res.hasError()) {
                    Utils.handleAuthError(MainActivity.this, findViewById(android.R.id.content), res);
                    return;
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(MainActivity.this, R.string.invites_sync_error, Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                    return;
                }

                AppDao dao = AppDatabaseFactory.getDatabase(MainActivity.this).appDao();
                List<Invite> invitesInDb = dao.getAllInvites().getValue();

                if (invitesInDb != null) {
                    long[] inviteIdsToDelete = new long[invitesInDb.size()];

                    int cnt = 0;
                    top_loop:
                    for (Invite invite : invitesInDb) {
                        if (res.getResult() != null)
                            for (Invite inviteFromAPI : res.getResult()) {
                                if (invite.getId() == inviteFromAPI.getId()) {
                                    continue top_loop;
                                }
                            }

                        inviteIdsToDelete[cnt++] = invite.getId();
                    }

                    inviteIdsToDelete = Arrays.copyOf(inviteIdsToDelete, cnt);
                    dao.deleteInvitesById(inviteIdsToDelete);
                }

                // TODO: 24.08.2018 Insert matched with contacts

                getSharedPreferences("opts", MODE_PRIVATE).edit()
                        .putLong("last_invites_update", System.currentTimeMillis())
                        .apply();
            }
        });
    }

    public void onOpenPrivacyPolicyClick(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacy_policy_url)));
        startActivity(intent);
    }
}

class ContactsCursorRecyclerViewAdapter extends RecyclerView.Adapter<ContactsCursorRecyclerViewAdapter.ViewHolder> {
    private Cursor mCursor;
    private int mNameColumnIdx, mIdColumnIdx;
    private LiveData<List<Invite>> mInvites;
    private OnItemClickListener mListener, mExcludeListener;
    private String mHighlight = "";

    private Observer<List<Invite>> invitesObserver = new Observer<List<Invite>>() {
        @Override
        public void onChanged(@Nullable List<Invite> invites) {
            notifyDataSetChanged();
        }
    };

    ContactsCursorRecyclerViewAdapter(Cursor cursor, int idColumnIdx, int nameColumnIdx, LiveData<List<Invite>> invites, OnItemClickListener listener, OnItemClickListener excludeListener) {
        swapCursor(cursor);
        swapInvitesData(invites);
        mNameColumnIdx = nameColumnIdx;
        mIdColumnIdx = idColumnIdx;
        mListener = listener;
        mExcludeListener = excludeListener;

        setHasStableIds(true);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        mInvites.removeObserver(invitesObserver);
        invitesObserver = null;
    }

    @NonNull
    @Override
    public ContactsCursorRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactsCursorRecyclerViewAdapter.ViewHolder holder, int position) {
        if (mCursor == null) return;
        if (!mCursor.moveToPosition(position)) return;

        String name = mCursor.getString(mNameColumnIdx);
        int start = name.toLowerCase().indexOf(mHighlight.toLowerCase());
        int end = start + mHighlight.length();
        SpannableString spannable = new SpannableString(name);
        if (start >= 0)
            spannable.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        holder.mNameView.setText(spannable);

        Invite invite = null;
        if (mInvites.getValue() != null) {
            for (Invite inv : mInvites.getValue()) {
                if (inv.getContactId() == mCursor.getLong(mIdColumnIdx)) {
                    invite = inv;
                    break;
                }
            }
        }

        holder.mInvitedIconView.setVisibility(invite != null && invite.getInvited() ? View.VISIBLE : View.GONE);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onItemClick(mCursor, view, holder.getAdapterPosition());
            }
        });

        holder.mIgnoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mExcludeListener != null)
                    mExcludeListener.onItemClick(mCursor, view, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCursor == null || mCursor.isClosed() ? 0 : mCursor.getCount();
    }

    @Override
    public long getItemId(int position) {
        if (mCursor == null || mCursor.isClosed())
            return -1;

        mCursor.moveToPosition(position);
        return mCursor.getLong(mIdColumnIdx);
    }

    public void swapCursorAndInvites(Cursor newCursor, LiveData<List<Invite>> newInvites) {
        swapCursor(newCursor);
        swapInvitesData(newInvites);
    }

    public void swapCursor(Cursor newCursor) {
        if (mCursor == newCursor) return;

//        Cursor oldCursor = mCursor;
//        if (oldCursor != null) {
//            if (mChangeObserver != null) oldCursor.unregisterContentObserver(mChangeObserver);
//            if (mDataSetObserver != null) oldCursor.unregisterDataSetObserver(mDataSetObserver);
//        }

        mCursor = newCursor;
        if (newCursor != null) {
            notifyDataSetChanged();
        } else {
            // notify observers about lack of a data set
            notifyDataSetChanged();
        }

    }

    public void swapInvitesData(LiveData<List<Invite>> newInvites) {
        if (mInvites == newInvites) return;
        if (mInvites != null) mInvites.removeObserver(invitesObserver);
        if (newInvites != null) newInvites.observeForever(invitesObserver);
        mInvites = newInvites;
        notifyDataSetChanged();
    }

    public void setHighlight(String highlight) {
        mHighlight = highlight;
    }

    interface OnItemClickListener {
        void onItemClick(Cursor cursor, View view, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View mView, mBackgroundView, mForegroundView;
        TextView mNameView;
        ImageView mInvitedIconView;
        Button mIgnoreBtn;

        ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mNameView = itemView.findViewById(R.id.name);
            mInvitedIconView = itemView.findViewById(R.id.icon);
            mBackgroundView = itemView.findViewById(R.id.backgroundView);
            mForegroundView = itemView.findViewById(R.id.foregroundView);
            mIgnoreBtn = itemView.findViewById(R.id.ignoreBtn);
        }
    }

    static class CustomRecycleViewTouchHelper extends ItemTouchHelper.Callback {
        private RecyclerView.SimpleOnItemTouchListener itemTouchListener = new RecyclerView.SimpleOnItemTouchListener(){
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN){
                    View viewUnderTouch = rv.findChildViewUnder(e.getX(), e.getY());
                    if (viewUnderTouch != null) {
                        ViewHolder holder = (ViewHolder) rv.getChildViewHolder(viewUnderTouch);
                        if (holder != null && holder.mIgnoreBtn.getVisibility() != View.INVISIBLE){
                            int x = holder.mView.getWidth() - holder.mIgnoreBtn.getWidth();
                            if (e.getX() >= x){
                                holder.mIgnoreBtn.performClick();
                                return true;
                            }
                        }
                    }
                }

                return false;
            }
        };

        CustomRecycleViewTouchHelper(RecyclerView rv){
            rv.addOnItemTouchListener(itemTouchListener);
        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            int dragFlags = 0;
            int swipeFlags = ItemTouchHelper.START;
            return makeMovementFlags(dragFlags, swipeFlags);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

         @Override
         public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

         }

        @Override
        public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;
            return (float) holder.mView.getWidth() / (float) holder.mIgnoreBtn.getWidth();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            ViewHolder holder = (ViewHolder) viewHolder;
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                holder.mIgnoreBtn.setVisibility(dX != 0f ? View.VISIBLE : View.INVISIBLE);
                float rel = (float) holder.mIgnoreBtn.getWidth() / (float) holder.mForegroundView.getWidth();
                dX = dX * rel;
            }

    //        holder.mForegroundView.setTranslationX(dX);
            getDefaultUIUtil().onDraw(c, recyclerView, holder.mForegroundView, dX, dY, actionState, isCurrentlyActive);
        }


        @Override
        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;

            holder.mIgnoreBtn.setVisibility(View.INVISIBLE);
            getDefaultUIUtil().clearView(holder.mForegroundView);
        }

    }
}

