package com.unet.inviter;

import android.os.AsyncTask;

import com.unet.inviter.db.Invite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GetInvitesTask extends AsyncTask<GetInvitesTask.ParamsTuple, Integer, AsyncTaskResult<ArrayList<Invite>>> {
    private static final String TAG = "GetInvitesTask";
    private static final String URI_PREFIX = "invitations/";

    public static class ParamsTuple {
        public String resourceUri;
        public String accessToken;
        public long siteId;

        public ParamsTuple(String resourceUri, String accessToken, long siteId) {
            this.resourceUri = resourceUri;
            this.accessToken = accessToken;
            this.siteId = siteId;
        }
    }

    public static class Result {
        public boolean hasError = false;
        public ArrayList<Invite> invites;
        public String error;
        public String errorDescription;

        Result(String error, String errorDescription){
            this(error);
            this.errorDescription = errorDescription;
        }

        Result(String error){
            hasError = true;
            this.error = error;
        }

        Result(ArrayList<Invite> invites){
            hasError = false;
            this.invites = invites;
        }
    }

    @Override
    protected AsyncTaskResult<ArrayList<Invite>> doInBackground(ParamsTuple ...params){
        ArrayList<Invite> invites = new ArrayList<>();
        OkHttpClient client = new OkHttpClient();

        HashMap defaultParams = new HashMap();
        String next_uri = null;

        ParamsTuple param = params[0];

        do {
            RequestBody body;
            JSONObject jsonObject = new JSONObject(defaultParams);

            if (next_uri != null){
                try {
                    jsonObject.put("cursor", next_uri);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());

            Request request = new Request.Builder()
                    .url(param.resourceUri + URI_PREFIX)
                    .addHeader("Authorization", String.format("Bearer %s", param.accessToken))
                    .post(body)
                    .build();

            try {
                Response response = client.newCall(request).execute();
                String jsonBody = response.body() != null ? response.body().string() : null;
                JSONObject responseData = new JSONObject(jsonBody);

                /* Fail */
                if (!response.isSuccessful() || responseData.has("error")){
                    return new AsyncTaskResult<>(
                            response.code(),
                            responseData.optString("error", response.message()),
                            responseData.optString("error_description")
                    );
                }
                /* --- */

                JSONObject result = new JSONObject(jsonBody).optJSONObject("result");
                if (result == null) break;

                if (result.has("next_cursor") && !result.isNull("next_cursor")){
                    next_uri = result.optString("next_cursor");
                }

                JSONArray items = result.optJSONArray("items");
                for (int i=0; i<items.length(); i++){
                    JSONObject item = items.getJSONObject(i);
                    if (item == null) continue;
                    if (!item.getString("type").equals(Invite.TYPE_PHONE)) continue;
                    if (item.getString("status").equals(Invite.STATUS_DELETED)) continue;

                    invites.add(new Invite(
                            item.getLong("id"),
                            item.getString("label"),
                            item.getString("identifier"),
                            item.getString("link"),
                            item.getString("type"),
                            item.getString("status"),
                            item.getLong("used_uid"),
                            param.siteId
                    ));
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } while (next_uri != null && !isCancelled());

        return new AsyncTaskResult<>(invites);
    }
}
