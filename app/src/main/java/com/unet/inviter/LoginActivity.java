package com.unet.inviter;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import net.openid.appauth.AppAuthConfiguration;
import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.ClientAuthentication;
import net.openid.appauth.ResponseTypeValues;
import net.openid.appauth.TokenResponse;
import net.openid.appauth.browser.BrowserBlacklist;
import net.openid.appauth.browser.Browsers;
import net.openid.appauth.browser.VersionRange;
import net.openid.appauth.browser.VersionedBrowserMatcher;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";

    private CustomTabsIntent mCustomTabsIntent;
    private AuthorizationService mAuthService;
    private AuthorizationRequest mAuthRequest;

    private ViewSwitcher mBtnProgressSwitcher;
    private AuthStateManager mStateManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AuthorizationServiceConfiguration serviceConfig = new AuthorizationServiceConfiguration(
                Uri.parse(getString(R.string.api_authorization_endpoint)),
                Uri.parse(getString(R.string.api_token_endpoint))
        );

        AuthorizationRequest.Builder authRequestBuilder =
                new AuthorizationRequest.Builder(
                        serviceConfig,
                        getString(R.string.api_client_id),
                        ResponseTypeValues.CODE,
                        Uri.parse(getString(R.string.api_redirect_uri))
                );

        mAuthRequest = authRequestBuilder
                .setScopes("user_details", "user_sites")
                .build();

        AppAuthConfiguration authConfiguration = new AppAuthConfiguration.Builder()
                .setBrowserMatcher(new BrowserBlacklist(new VersionedBrowserMatcher(
                        Browsers.SBrowser.PACKAGE_NAME,
                        Browsers.SBrowser.SIGNATURE_HASH,
                        true,
                        VersionRange.atMost("5.3")
                )))
                .build();

        mAuthService = new AuthorizationService(this, authConfiguration);
        mStateManager = AuthStateManager.getInstance(this);

        /*PrivacyTermsActionCallback callback = new PrivacyTermsActionCallback() {
            @Override
            public void onAccepted() {
                finishInit();
            }

            @Override
            public void onDenied() {
                finishAffinity();
                finish();
            }
        };

        if (!ensurePrivacyTermsAccepted(callback)){
            return;
        }*/

        finishInit();
    }

    private void finishInit(){
        boolean authorized = mStateManager.getCurrent().isAuthorized();

        if (authorized){
            String name = getSharedPreferences("user_info", MODE_PRIVATE).getString("first_name", null);
            if (name != null){
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                return;
            }
        }

        setContentView(R.layout.activity_login);
        mBtnProgressSwitcher = findViewById(R.id.btn_progress_switcher);

        if (authorized) {
            updateUserInfoAndGo();
        }
    }

    @Override
    protected void onNewIntent(Intent intent){
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume(){
        super.onResume();

        mCustomTabsIntent = mAuthService.getCustomTabManager().createTabBuilder(mAuthRequest.toUri())
                .setStartAnimations(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .setExitAnimations(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            mCustomTabsIntent.intent.putExtra(Intent.EXTRA_REFERRER, Uri.parse(Intent.URI_ANDROID_APP_SCHEME + "//" + getPackageName()));
        }

        AuthorizationResponse resp = AuthorizationResponse.fromIntent(getIntent());
        AuthorizationException ex = AuthorizationException.fromIntent(getIntent());

        if (resp != null){
            onAuthorizationResponse(resp, ex);
        }

        if (ex != null) {
            showError(ex);
        }
    }

    public void onClick(View view){
        PendingIntent completionIntent = PendingIntent.getActivity(this, 0, new Intent(this, LoginActivity.class), 0);
        PendingIntent cancellationIntent = PendingIntent.getActivity(this, 0, new Intent(this, LoginActivity.class), 0);
        mAuthService.performAuthorizationRequest(mAuthRequest, completionIntent, cancellationIntent, mCustomTabsIntent);
    }

    private void onAuthorizationResponse(AuthorizationResponse resp, AuthorizationException ex) {
        showLoader();

        mStateManager.updateAfterAuthorization(resp, ex);

        ClientAuthentication clientAuthentication;
        try {
            clientAuthentication = mStateManager.getCurrent().getClientAuthentication();
        } catch (ClientAuthentication.UnsupportedAuthenticationMethod exception){
            showError(exception.getMessage());
            return;
        }

        mAuthService.performTokenRequest(resp.createTokenExchangeRequest(), clientAuthentication, new AuthorizationService.TokenResponseCallback() {
            @Override
            public void onTokenRequestCompleted(@Nullable TokenResponse response, @Nullable AuthorizationException ex) {
                mStateManager.updateAfterTokenResponse(response, ex);
                hideLoader();

                if (ex != null){
                    showError(ex);
                }

                if (response != null){
                    updateUserInfoAndGo();
                }
            }
        });
    }

    private void updateUserInfoAndGo(){
        showLoader();

        mStateManager.getCurrent().performActionWithFreshTokens(mAuthService, new AuthState.AuthStateAction() {
            @Override
            public void execute(@Nullable String accessToken, @Nullable String idToken, @Nullable AuthorizationException ex) {
                if (ex != null){
                    showError(ex);
                    hideLoader();
                    return;
                }

                mStateManager.update();

                try {
                    AsyncTaskResult<JSONObject> result = new GetUserInfoTask().execute(new GetUserInfoTask.ParamsTuple(
                            mStateManager.getCurrent().getLastTokenResponse().additionalParameters.get("resource_uri"),
                            accessToken
                    )).get();

                    hideLoader();

                    if (result.hasError()){
                        Toast.makeText(LoginActivity.this, R.string.api_error, Toast.LENGTH_SHORT)
                                .show();
                    }

                    JSONObject userInfo = result.getResult().optJSONObject("result");
                    if (userInfo != null){
                        SharedPreferences.Editor prefsEditor = getSharedPreferences("user_info", MODE_PRIVATE).edit();

                        prefsEditor.putString("first_name", userInfo.optString("first_name"));
                        prefsEditor.putString("last_name", userInfo.optString("last_name"));
                        prefsEditor.putString("profile_url", userInfo.optString("profile_url"));
                        JSONArray photos = userInfo.optJSONArray("face_photos");
                        if (photos != null){
                            JSONObject photo = photos.optJSONObject(0);
                            prefsEditor.putString("photo_url", photo.optString("url"));
                        }

                        prefsEditor.apply();

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                finally {
                    hideLoader();
                }
            }
        });
    }

    private void showLoader(){
        mBtnProgressSwitcher.showNext();
    }
    private void hideLoader(){
        mBtnProgressSwitcher.showPrevious();
    }

    private void showError(@NonNull AuthorizationException ex) {
        String errorText;

        AuthorizationException userCancel = AuthorizationException.GeneralErrors.USER_CANCELED_AUTH_FLOW;
        AuthorizationException programCancel = AuthorizationException.GeneralErrors.PROGRAM_CANCELED_AUTH_FLOW;

        if ((userCancel.code == ex.code && userCancel.type == ex.type) || (programCancel.code == ex.code && userCancel.type == ex.type) ){
            errorText = "Authorization canceled";
        }
        else if (ex.errorDescription != null && ex.errorDescription.length() != 0){
            errorText = ex.errorDescription;
        }
        else if (ex.error != null) {
            errorText = ex.error;
        }
        else {
            errorText = "Unknown Error";
        }

        showError(errorText);
    }
    private void showError(@NonNull String errorText){
        TextView textView = findViewById(R.id.hint_text);
        textView.setText(errorText);
        textView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuthService.dispose();
    }

    static class GetUserInfoTask extends AsyncTask<GetUserInfoTask.ParamsTuple, Void, AsyncTaskResult<JSONObject>> {
        static class ParamsTuple {
            String resourceUri;
            String accessToken;

            ParamsTuple(String resourceUri, String accessToken) {
                this.resourceUri = resourceUri;
                this.accessToken = accessToken;
            }
        }

        @Override
        protected AsyncTaskResult<JSONObject> doInBackground(ParamsTuple... params) {
            ParamsTuple param = params[0];
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(param.resourceUri + "user/")
                    .addHeader("Authorization", String.format("Bearer %s", param.accessToken))
                    .build();

            try {
                Response response = client.newCall(request).execute();

                String jsonBody = response.body() != null ? response.body().string() : null;
                JSONObject responseData = new JSONObject(jsonBody);

                if (!response.isSuccessful() || responseData.has("error")){
                    return new AsyncTaskResult<>(
                            response.code(),
                            responseData.optString("error", response.message()),
                            responseData.optString("error_description")
                    );
                }
//                if (!response.isSuccessful()){
//                    return new AsyncTaskResult<>(response.code(), response.message(), null);
//                }

                return new AsyncTaskResult<>(new JSONObject(jsonBody));
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return new AsyncTaskResult<>("error", null);
        }
    }

    /*interface PrivacyTermsActionCallback {
        void onAccepted();
        void onDenied();
    }*/

    /**
     * Check if user has accepted latest privacy terms and if not show dialog with privacy terms to
     * accept
     *
     * @param callback Callback to be notified about user actions to privacy dialog
     * @return true - if user has accepted privacy terms and false if not and dialog will be showed
     */
    /*boolean ensurePrivacyTermsAccepted(final PrivacyTermsActionCallback callback){
        final int termsVersion = BuildConfig.TERMS_VERSION;
        int confirmedTermsVersion = getSharedPreferences("opts", MODE_PRIVATE).getInt("terms_confirmed", 0);
        if (confirmedTermsVersion == termsVersion) return true;

        DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Dialog.BUTTON_POSITIVE:
                        getSharedPreferences("opts", MODE_PRIVATE).edit().putInt("terms_confirmed", termsVersion).apply();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                callback.onAccepted();
                            }
                        });
                        break;
                    case Dialog.BUTTON_NEGATIVE:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                callback.onDenied();
                            }
                        });
                        break;
                }
            }
        };

        new AlertDialog.Builder(this)
                .setTitle(R.string.privacy_policy_title)
                .setMessage(R.string.privacy_policy)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, clickListener)
                .setPositiveButton(R.string.accept, clickListener)
                .create().show();

        return false;
    }*/
}
