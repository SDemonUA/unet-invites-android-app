package com.unet.inviter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;

public final class Utils {
    /**
     * Checks if errorMessage is and auth error and shows Toast or Snackbar if possible to user with
     * re-login action
     *
     * @param context Application context
     * @param view View needed for Snackbar to be shown - if view is null, Toast will be used
     * @param errorMessage Error message to examine
     * @return true if error is and auth error and false otherwise
     */
    static void handleAuthError(Context context, View view, String errorMessage){
        showApiError(context, view, errorMessage != null && errorMessage.equalsIgnoreCase("Unauthorized"));
    }

    static void handleAuthError(Context context, View view, AuthorizationException ex){
        showApiError(context, view, ex != null);
    }

    static void handleAuthError(Context context, View view, AsyncTaskResult res) {
        int statusCode = res == null ? 500 : res.getStatusCode();
        showApiError(context, view, statusCode == 401);
    }

    private static void showApiError(final Context context, View view, boolean isAuthError){
        final int messageRes = isAuthError ? R.string.auth_error : R.string.api_error;

        if (view == null){
            new Handler(context.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast toast = Toast.makeText(context, messageRes, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.BOTTOM, 0, 0);
                    toast.show();
                }
            });

            return;
        }

        Snackbar snackbar = Snackbar.make(view, messageRes, Snackbar.LENGTH_LONG);

        if (isAuthError) snackbar.setAction(R.string.relogin, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AuthStateManager.getInstance(context).replace(new AuthState());
                context.getSharedPreferences("user_info", Activity.MODE_PRIVATE).edit().clear().apply();
                context.startActivity(new Intent(context, LoginActivity.class));
            }
        });

        snackbar.show();
    }

}
