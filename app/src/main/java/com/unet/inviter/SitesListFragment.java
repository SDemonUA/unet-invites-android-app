package com.unet.inviter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.unet.inviter.db.AppDao;
import com.unet.inviter.db.AppDatabase;
import com.unet.inviter.db.AppDatabaseFactory;
import com.unet.inviter.db.Site;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SitesListFragment extends Fragment {
    public static final String ARG_COLUMN_COUNT = "column-count";
    public static final String ARG_SELECTED_ID = "selected-id";
    private static final String TAG = "SitesListFragment";
    ListViewState mListViewState = ListViewState.LOADED;
    private int mColumnCount = 1;
    private long mSelectedId = -1;
    private OnListFragmentInteractionListener mListener;
    private SiteRecyclerViewAdapter.OnListItemInteractionListener mListListener;
    private LiveData<List<Site>> sitesLiveData;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SitesListFragment() {
    }

    @SuppressWarnings("unused")
    public static SitesListFragment newInstance(int columnCount) {
        SitesListFragment fragment = new SitesListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(ARG_SELECTED_ID, mSelectedId);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mListListener = new SiteRecyclerViewAdapter.OnListItemInteractionListener() {
            @Override
            public void onClick(Site item) {
                mSelectedId = item != null ? item.getId() : -1;
                mListener.onListFragmentInteraction(item);
            }
        };

        if (savedInstanceState != null) {
            mSelectedId = savedInstanceState.getLong(ARG_SELECTED_ID, mSelectedId);
        }

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            mSelectedId = getArguments().getLong(ARG_SELECTED_ID, mSelectedId);
        }

        sitesLiveData = AppDatabaseFactory.getDatabase(getContext()).appDao().getAllSites();

//        SharedPreferences prefs = getContext().getSharedPreferences("opts", Context.MODE_PRIVATE);
//        long lastUpdateTime = prefs.getLong("last_sites_update", 0);
//        if (System.currentTimeMillis() - lastUpdateTime > 10 * 60 * 1000 ||
//                sitesLiveData.getValue() == null ||
//                sitesLiveData.getValue().size() == 0) { // Not sooner then 10 minutes
            fetchSites();
//        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sites_list, container, false);
        View listView = view.findViewById(R.id.list);
        ImageButton updateBtn = view.findViewById(R.id.empty_communities_list_btn);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchSites();
            }
        });

        // Set the adapter
        if (listView instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) listView;
            if (mColumnCount <= 1) {
                LinearLayoutManager lm = new LinearLayoutManager(context);
                recyclerView.setLayoutManager(lm);
                recyclerView.addItemDecoration(new DividerItemDecoration(context, lm.getOrientation()));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            recyclerView.setAdapter(new SiteRecyclerViewAdapter(sitesLiveData, mListListener, mSelectedId));
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setListViewState(mListViewState);

        long selectedCommunityId = ((MainActivity) getActivity()).getSelectedCommunityId();
        if (selectedCommunityId >= 0) {
            mSelectedId = selectedCommunityId;
            View view = getView();
            if (view != null) {
                RecyclerView rv = view.findViewById(R.id.list);
                ((SiteRecyclerViewAdapter) rv.getAdapter()).setSelectedId(mSelectedId);
            }
        }

        sitesLiveData.observe(this, new Observer<List<Site>>() {
            @Override
            public void onChanged(@Nullable List<Site> sites) {
                if (getView() == null) return;
                boolean showEmpty = (sites == null || sites.size() == 0);

                getView().findViewById(R.id.empty_communities_list).setVisibility(showEmpty ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    enum ListViewState {LOADING, LOADED, FAILED}
    private void setListViewState(final ListViewState state) {
        final View view = getView();

        mListViewState = state;

        if (view == null) return;

        view.post(new Runnable() {
            @Override
            public void run() {
                View list = view.findViewById(R.id.list_container);
                View error = view.findViewById(R.id.load_communities_error);
                View progress = view.findViewById(R.id.progress);
                View updateBtn = view.findViewById(R.id.empty_communities_list_btn);

                list.setVisibility(state == ListViewState.LOADED ? View.VISIBLE : View.GONE);
                error.setVisibility(state == ListViewState.FAILED ? View.VISIBLE : View.GONE);

                boolean showProgress = state == ListViewState.LOADING;
                progress.setVisibility(showProgress ? View.VISIBLE : View.GONE);
                updateBtn.setVisibility(!showProgress ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void fetchSites() {
        if (getActivity() == null) return;

        setListViewState(ListViewState.LOADING);

        new Thread(new Runnable() {
            @Override
            public void run() {
                AuthState authState = AuthStateManager.getInstance(getActivity()).getCurrent();
                final String resource_uri = authState.getLastTokenResponse().additionalParameters.get("resource_uri");
                final AuthorizationService authService = new AuthorizationService(getActivity());

                authState.performActionWithFreshTokens(authService, new AuthState.AuthStateAction() {
                    @Override
                    public void execute(@Nullable final String accessToken, @Nullable String idToken, @Nullable AuthorizationException ex) {
                        authService.dispose();

                        if (ex != null) {
                            setListViewState(ListViewState.FAILED);
                            Utils.handleAuthError(getContext(), getView(), ex);
//                            Snackbar.make(getView(), R.string.api_error, Snackbar.LENGTH_INDEFINITE)
//                                    .setAction(R.string.relogin, new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            AuthStateManager.getInstance(getActivity()).replace(new AuthState());
//                                            getActivity().getSharedPreferences("user_info", Activity.MODE_PRIVATE).edit().clear().apply();
//                                            startActivity(new Intent(getActivity(), LoginActivity.class));
//                                        }
//                                    }).show();
                            return;
                        }

                        AuthStateManager.getInstance(getActivity()).update();

                        GetSitesTask task = new GetSitesTask();

                        try {
                            final AsyncTaskResult<ArrayList<Site>> fetchResult = task.execute(resource_uri, accessToken).get();

                            if (fetchResult.hasError()) {
                                setListViewState(ListViewState.FAILED);
                                Utils.handleAuthError(getContext(), getView(), fetchResult);
                                return;
                            }

                            final List<Site> sites = fetchResult.getResult();

                            if (getContext() == null){
                                setListViewState(ListViewState.FAILED);
                                return;
                            }

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    AppDatabase db = AppDatabaseFactory.getDatabase(getContext());
                                    AppDao dao = db.appDao();

                                    long[] cachedSiteIds = dao.getAllSiteIds();
                                    long[] removedSiteIds = new long[cachedSiteIds.length];
                                    long[] updatedSiteIds = new long[cachedSiteIds.length]; // Ids of sites that exists in local and external DB

                                    // If site is not present in server response - then it should be removed from cache (local DB)
                                    int cnt = 0, updCnt = 0;
                                    top_loop:
                                    for (long id : cachedSiteIds) {
                                        for (Site site : sites) {
                                            if (site.getId() == id) {
                                                updatedSiteIds[updCnt++] = id;
                                                continue top_loop;
                                            }
                                        }

                                        removedSiteIds[cnt++] = id;
                                    }

                                    removedSiteIds = Arrays.copyOf(removedSiteIds, cnt); // Trim to actual size
                                    updatedSiteIds = Arrays.copyOf(updatedSiteIds, updCnt);
                                    if (removedSiteIds.length != 0)
                                        dao.deleteSitesById(removedSiteIds);

                                    // Clean up "excluded_contacts" array for deleted sites
                                    SharedPreferences opts = getContext().getSharedPreferences("opts", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = opts.edit();

                                    for (long id : removedSiteIds) {
                                        editor.remove("excluded_contacts" + id);
                                        editor.remove("inv_message_preset_"+id);

                                        if (id == mSelectedId){
                                            getView().post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    RecyclerView rv = getView().findViewById(R.id.list);
                                                    ((SiteRecyclerViewAdapter) rv.getAdapter()).setSelectedId(-1);
                                                    mListener.onListFragmentInteraction(null);
                                                }
                                            });
                                        }
                                    }

                                    editor.apply();
                                    // ---

                                    dao.insertAllSites(sites.toArray(new Site[]{})); // Merge sites into DB

                                    // Update
                                    for (Site site : sites) {
                                        for (long siteId : updatedSiteIds) {
                                            if (site.getId() == siteId) {
                                                dao.updateSite(site);
                                                break;
                                            }
                                        }
                                    }

                                    // Store last update time
                                    opts.edit().putLong("last_sites_update", System.currentTimeMillis()).apply();

                                    setListViewState(ListViewState.LOADED);
                                }
                            }).start();

                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                            setListViewState(ListViewState.FAILED);
                        }
                    }
                });
            }
        }).start();
    }



    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Site item);
    }

    static class GetSitesTask extends AsyncTask<String, Void, AsyncTaskResult<ArrayList<Site>>> {
        @Override
        protected AsyncTaskResult<ArrayList<Site>> doInBackground(String... args) {
            String resource_uri = args[0];
            String accessToken = args[1];

            ArrayList<Site> sites = new ArrayList<>();
            OkHttpClient client = new OkHttpClient();

            HashMap<String, Object> defaultParams = new HashMap<>();
            defaultParams.put("is_admin", true);
            defaultParams.put("type", "community");

            String next_uri = null;
            do {
                RequestBody body;
                JSONObject jsonObject = new JSONObject(defaultParams);

                if (next_uri != null) {
                    try {
                        jsonObject.put("cursor", next_uri);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());

                Request request = new Request.Builder()
                        .url(resource_uri + "sites/")
                        .addHeader("Authorization", String.format("Bearer %s", accessToken))
                        .post(body)
                        .build();

                try {
                    Response response = client.newCall(request).execute();

                    String jsonBody = response.body() != null ? response.body().string() : null;
                    JSONObject responseData = new JSONObject(jsonBody);

                    // Error!
                    if (!response.isSuccessful() || responseData.has("error")){
                        return new AsyncTaskResult<>(
                                response.code(),
                                responseData.optString("error", response.message()),
                                responseData.optString("error_description")
                        );
                    }

                    JSONObject result = responseData.optJSONObject("result");
                    if (result == null) break;

                    if (result.has("next_cursor") && !result.isNull("next_cursor")) {
                        next_uri = result.optString("next_cursor");
                    }

                    JSONArray items = result.optJSONArray("items");
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject item = items.getJSONObject(i);
                        if (item == null) continue;

                        if (!item.getBoolean("is_admin")) continue;
                        if (!item.getString("type").equals("community")) continue;

                        sites.add(new Site(
                                item.getLong("id"),
                                item.getString("title"),
                                item.getString("url"),
                                item.getString("logo_url"),
                                item.getString("target_resource_uri")
                        ));
                    }
                } catch (Exception exception) {
                    return new AsyncTaskResult<>("Error", null);
                }

            } while (next_uri != null && !isCancelled());

            return new AsyncTaskResult<>(sites);
        }
    }
}
