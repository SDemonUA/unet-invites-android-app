package com.unet.inviter;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.unet.inviter.db.AppDao;
import com.unet.inviter.db.AppDatabase;
import com.unet.inviter.db.AppDatabaseFactory;
import com.unet.inviter.db.Invite;
import com.unet.inviter.db.Site;
import com.unet.inviter.services.ApiServiceContract;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MessageActivity extends AppCompatActivity {
    public static final String EXTRA_COMMUNITY_ID = "community_id";
    public static final String EXTRA_COMMUNITY_TITLE = "community_title";

    public static final String EXTRA_CONTACT_KEY = "contact_key";
    public static final String EXTRA_CONTACT_NAME = "contact_name";
    public static final String EXTRA_CONTACT_DISPLAY_NAME = "contact_display_name";
    public static final String EXTRA_CONTACT_ID = "contact_id";

    public static final String EXTRA_NO_CLIP = "no_clip";
    private static final int SEND_INVITE = 10000;

    private InputRecord mInfo;
    private Invite mInvite;
    private Site mSite;
    private String mMessagePreset = "";

    private ListView mActionsListView;
    private TextView mEditorView;
    private FrameLayout mProgressView;
    private ProgressBar mProgressActionsListView;

    private ExecutorService executor = Executors.newSingleThreadExecutor();

    private boolean mInvited = false; // Not approved saved to local/remote db invited state (tmp)

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver (){
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) return;

            switch (intent.getAction()){
                case ApiServiceContract.BROADCAST_INVITE_STATUS:
                    Log.d("BroadcastReceiver", "got answer from intent service");

                    ApiServiceContract.Status status =
                            ApiServiceContract.Status.valueOf(intent.getStringExtra(ApiServiceContract.EXTRA_STATUS));

                    if (ApiServiceContract.Status.DONE == status){
                        if (mInvite != null) {
                            mInvite.setInvited(true);
                            invalidateOptionsMenu();
                        }
                    }
                    else if(ApiServiceContract.Status.ERROR == status) {
                        ApiServiceContract.Error error =
                                ApiServiceContract.Error.valueOf(intent.getStringExtra(ApiServiceContract.EXTRA_STATUS));
                        String errorMessage = intent.getStringExtra(ApiServiceContract.EXTRA_ERROR_MESSAGE);

                        if (error == ApiServiceContract.Error.AUTH_ERROR){
                            Utils.handleAuthError(MessageActivity.this, findViewById(android.R.id.content), errorMessage);
                            return;
                        }
                        else {
                            Log.d("BroadcastReceiver", "onReceive: "+intent);
                        }

                    }


                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        initViews();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back_btn);
            actionBar.setHomeButtonEnabled(true);
        }

        mInfo = parseIntent(getIntent());
        assert mInfo != null;

        SharedPreferences opts = getSharedPreferences("opts", MODE_PRIVATE);
        mMessagePreset = opts.getString("inv_message_preset_"+mInfo.communityID, getResources().getString(R.string.sample_invite_msg).replace("$community_name", mInfo.communityTitle));

        setTitle(mInfo.contactDisplayName);

        IntentFilter statusIntentFilter = new IntentFilter(ApiServiceContract.BROADCAST_INVITE_STATUS);
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, statusIntentFilter);

        // Do not set message text until we get all replacements for it
        // mEditorView.setText(mMessagePreset);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_messages_menu, menu);
        menu.setGroupVisible(R.id.status, mInvite != null && mInvite.getInvited() || mInvited);

        menu.getItem(0).setIcon((mInvite != null && mInvite.getInvited()) ? R.drawable.ic_baseline_done : R.drawable.ic_baseline_done_gray);
        return true;
    }

    private void updateViews(final boolean isLoading){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressView.setVisibility(isLoading ? View.VISIBLE : View.GONE);

                if (mInfo != null){
                    setTitle(mInfo.contactDisplayName);
                }

                setupMessageEditor();
                setupActionsList();
            }
        });
    }

    private void initViews(){
        mEditorView = findViewById(R.id.text);
        mActionsListView = findViewById(R.id.actions_list);
        TextView emptyActionsListView = findViewById(R.id.empty_actions_list);
        mProgressActionsListView = findViewById(R.id.progress_actions_list);
        mProgressView = findViewById(R.id.progress);

        mActionsListView.setEmptyView(emptyActionsListView);
        mActionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                HashMap item = (HashMap) adapterView.getItemAtPosition(position);
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

                String message = mEditorView.getText().toString();
                final String TEXT_MESSAGE = message;
                final String HTML_MESSAGE = message.replace(mInvite.getLink(), "<a href=\""+mInvite.getLink()+"\">"+mInvite.getLink()+"</a>");

                try {
                    Intent intent = (Intent) item.get("intent");

                    intent.putExtra(Intent.EXTRA_TEXT, TEXT_MESSAGE);
                    intent.putExtra("sms_body", TEXT_MESSAGE);
                    intent.setClipData(new ClipData("message", new String[]{"text/plain", "text/html"}, new ClipData.Item(TEXT_MESSAGE, HTML_MESSAGE)));
                    intent.putExtra(Intent.EXTRA_HTML_TEXT, HTML_MESSAGE);

                    if (!intent.getBooleanExtra(EXTRA_NO_CLIP, false) && clipboard != null){
                        clipboard.setPrimaryClip(new ClipData("message", new String[]{"text/plain", "text/html"}, new ClipData.Item(TEXT_MESSAGE, HTML_MESSAGE)));

                        Toast toast = Toast.makeText(MessageActivity.this, R.string.msg_copied_into_clipboard, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }


                    ApiIntentService.startActionMarkAsInvited(MessageActivity.this, mSite.getResourceUri(), mInvite.getId());

                    startActivityForResult((Intent) item.get("intent"), SEND_INVITE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        emptyActionsListView.setVisibility(View.GONE);
        mProgressActionsListView.setVisibility(View.GONE);
    }

    private void setupMessageEditor(){
        if (mSite == null || mInvite == null){
            return;
        }

        String message = mMessagePreset;
        String name = mInfo.contactName;
        String link = mInvite.getLink();
        message = message.replace("$name", name);
        message = message.replace("$link", link);

        SpannableStringBuilder spannable = new SpannableStringBuilder(message);
        int nameStartIdx = message.indexOf(name);
        int linkStartIdx = message.indexOf(link);

        if (nameStartIdx >= 0){
            StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
            spannable.setSpan(styleSpan, nameStartIdx, nameStartIdx+name.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }

        if (linkStartIdx>= 0){
            ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#5770FF"));
            spannable.setSpan(colorSpan, linkStartIdx, linkStartIdx+link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        mEditorView.setText(spannable);
    }

    private class RawContact {
        long rawId, contactId;
        String type, name, dataSet;
        ArrayList<HashMap<String, String>> data;

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(String.format(Locale.getDefault(), "Contact ID: %d\tRaw ID: %d\nName: %s\tType: %s\nData Set: %s\n",
                    contactId, rawId, name, type, dataSet));

            if (data != null){
                for (HashMap<String, String> dataRow: data){

                    for (Map.Entry<String, String> entry :dataRow.entrySet()){
                        sb.append(entry.getKey());
                        sb.append(": ");
                        sb.append(entry.getValue());
                        sb.append("\n");
                    }
                    sb.append("---------------\n\n");
                }
            }

            return sb.toString();
        }
    }

    private void fetchRawContacts(){

        final ContentResolver cr = getContentResolver();

        executor.execute(new Runnable() {
            @Override
            public void run() {
                final ArrayList<RawContact> rawContacts = new ArrayList<>();


                Cursor cursor = cr.query(
                        ContactsContract.RawContacts.CONTENT_URI,
                        new String[]{
                                ContactsContract.RawContacts._ID,
                                ContactsContract.RawContacts.ACCOUNT_NAME,
                                ContactsContract.RawContacts.ACCOUNT_TYPE,
                                ContactsContract.RawContacts.DATA_SET
                        },
                        ContactsContract.RawContacts.CONTACT_ID + " = ?",
                        new String[]{String.valueOf(mInfo.contactID)},
                        null
                );

                if (cursor == null || cursor.getCount() == 0){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onFetchRawContacts(null);
                        }
                    });
                    return;
                }

                while(cursor.moveToNext()){
                    RawContact contact = new RawContact();
                    contact.contactId = mInfo.contactID;
                    contact.rawId = cursor.getLong(0);
                    contact.name = cursor.getString(1);
                    contact.type = cursor.getString(2);
                    contact.dataSet = cursor.getString(3);
                    contact.data = new ArrayList<>();
                    rawContacts.add(contact);


                    Uri contactUri = ContentUris.withAppendedId(ContactsContract.RawContacts.CONTENT_URI, contact.rawId);
                    Cursor dataCursor = cr.query(
                            Uri.withAppendedPath(contactUri, ContactsContract.RawContacts.Data.CONTENT_DIRECTORY),
                            new String[]{
                                    ContactsContract.RawContacts.Data._ID,
                                    ContactsContract.RawContacts.Data.DATA1,
                                    ContactsContract.RawContacts.Data.DATA2,
                                    ContactsContract.RawContacts.Data.DATA3,
                                    ContactsContract.RawContacts.Data.DATA4,
                                    ContactsContract.RawContacts.Data.DATA5,
                                    ContactsContract.RawContacts.Data.DATA6,
                                    ContactsContract.RawContacts.Data.DATA7,
                                    ContactsContract.RawContacts.Data.DATA8,
                                    ContactsContract.RawContacts.Data.DATA9,
                                    ContactsContract.RawContacts.Data.DATA10,
                                    ContactsContract.RawContacts.Data.DATA11,
                                    ContactsContract.RawContacts.Data.DATA12,
                                    ContactsContract.RawContacts.Data.DATA13,
                                    ContactsContract.RawContacts.Data.DATA14,
                                    ContactsContract.RawContacts.Data.MIMETYPE,
                            },
                            null,
                            null,
                            null
                    );

                    if (dataCursor == null){
                        continue;
                    }

                    int dataColumnCount = dataCursor.getColumnCount();

                    while( dataCursor.moveToNext() ){
                        HashMap<String, String> data = new HashMap<>();

                        for (int i=0; i<dataColumnCount; i++){
                            data.put(dataCursor.getColumnName(i), dataCursor.getString(i));
                        }

                        contact.data.add(data);
                    }

                    dataCursor.close();

                }

                cursor.close();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onFetchRawContacts(rawContacts);
                    }
                });
            }

        });

    }
    private void onFetchRawContacts(@Nullable ArrayList<RawContact> rawContacts){
        if (rawContacts == null){
            return;
        }

        PackageManager pm = getPackageManager();
        HashMap<Intent, List<ResolveInfo>> intentToResolvedInfo = new HashMap<>();

        String message = mEditorView.getText().toString();

        final String TEXT_MESSAGE = message;
        final String HTML_MESSAGE = message.replace(mInvite.getLink(), "<a href=\""+mInvite.getLink()+"\">"+mInvite.getLink()+"</a>");

        HashSet<String> usedPairs = new HashSet<>();

        for (RawContact contact: rawContacts){
            for (HashMap<String, String> dataRow : contact.data) {
                String mimeType = dataRow.get(ContactsContract.Data.MIMETYPE);
                if (
                        mimeType.contains("call") ||
                        mimeType.contains("video")||
                        mimeType.contains("voice")||
                        mimeType.contains("Video")||
                        mimeType.contains("Call") ||
                        mimeType.contains("Voice")
                    ) continue;

                Intent intent = new Intent();
                String data1 = dataRow.get(ContactsContract.Data.DATA1);
                String subTitle = data1;
                intent.putExtra("label", subTitle);

                if (usedPairs.contains(mimeType+data1)){
                    continue;
                }

                usedPairs.add(mimeType+data1);

                switch (mimeType) {
                    case ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE: {
                        intent.setAction(Intent.ACTION_SENDTO);
                        intent.setType("*/*");
                        intent.setData(Uri.parse("mailto:")); // only email apps should handle this

                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{dataRow.get(ContactsContract.CommonDataKinds.Email.ADDRESS)});
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Invitation");
                        intent.putExtra(EXTRA_NO_CLIP, true);

                        String label = dataRow.get(ContactsContract.CommonDataKinds.Email.LABEL);
                        if (label == null)
                            label = "(Email) " + dataRow.get(ContactsContract.CommonDataKinds.Email.ADDRESS);
                        intent.putExtra("label", label);
                        break;
                    }
                    case ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE: {
                        String phoneNumber = dataRow.get(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
                        if (phoneNumber == null) phoneNumber = dataRow.get(ContactsContract.CommonDataKinds.Phone.NUMBER);

                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setType("vnd.android-dir/mms-sms");
                        intent.setData(Uri.parse("smsto:" + phoneNumber));  // This ensures only SMS apps respond

                        intent.putExtra("address", phoneNumber);
                        intent.putExtra("sms_body", TEXT_MESSAGE);
                        intent.putExtra(EXTRA_NO_CLIP, true);

                        String label = dataRow.get(ContactsContract.CommonDataKinds.Phone.LABEL);
                        if (label == null) label = "(SMS) " + phoneNumber;

                        intent.putExtra("label", label);
                        break;
                    }
                    default: {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.setPackage(dataRow.get(ContactsContract.Data.RES_PACKAGE));
                        }

                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, Long.parseLong(dataRow.get(ContactsContract.Data._ID))));
                        intent.setClipData(new ClipData("message", new String[]{"text/plain", "text/html"}, new ClipData.Item(TEXT_MESSAGE, HTML_MESSAGE)));

                        String label = dataRow.get(ContactsContract.Data.DATA3);
                        if (label == null) label = dataRow.get(ContactsContract.Data.DATA1);
                        if (label == null) label = dataRow.get(ContactsContract.Data.DATA2);
                        intent.putExtra("label", label);

                        if (mimeType.contains("facebook")) { // Facebook set virtual intent that doesn't starts at all
                            intent.putExtra(EXTRA_NO_CLIP, true);
                            intent.setPackage("com.facebook.orca");
                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                        }
                        else if (mimeType.contains("whatsapp")){
                            try {
                                intent.setData(Uri.parse("https://wa.me/"+dataRow.get(ContactsContract.Data.DATA1)+"?text="+URLEncoder.encode(TEXT_MESSAGE, "UTF-8")));
                                intent.putExtra(EXTRA_NO_CLIP, true);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    }
                }

                if (intent.getStringExtra("label") == null){
                    intent.putExtra("label", "");
                }

                intent.putExtra(Intent.EXTRA_TEXT, TEXT_MESSAGE);
                intent.putExtra(Intent.EXTRA_HTML_TEXT, HTML_MESSAGE);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION);

                List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, PackageManager.GET_META_DATA);
                if (resolveInfos.size() == 0) continue;
                intentToResolvedInfo.put(intent, resolveInfos);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Intent intent = new Intent();
            intent.setAction(ContactsContract.Intents.ACTION_VOICE_SEND_MESSAGE_TO_CONTACTS);
            intent.setType("*/*");
            intent.putExtra(ContactsContract.Intents.EXTRA_RECIPIENT_CONTACT_NAME, new String[]{mInfo.contactName});
            intent.putExtra(ContactsContract.Intents.EXTRA_RECIPIENT_CONTACT_URI, new String[]{"content://com.android.contacts/contacts/"+mInfo.contactID});

            intent.putExtra("label", "");
            intent.putExtra(Intent.EXTRA_TEXT, TEXT_MESSAGE);
            intent.putExtra(Intent.EXTRA_HTML_TEXT, HTML_MESSAGE);

            List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, PackageManager.GET_META_DATA);
            if (resolveInfos.size() != 0)
                intentToResolvedInfo.put(intent, resolveInfos);
        }

        ArrayList<HashMap<String, Object>> activities = new ArrayList<>();
        HashMap<String, Drawable> icons = new HashMap<>();

        for (Map.Entry<Intent, List<ResolveInfo>> entry: intentToResolvedInfo.entrySet()){

            String scheme = entry.getKey().getScheme();

            for (ResolveInfo info: entry.getValue()){

                // Filter unwanted apps
                String packageName = info.activityInfo.packageName;

                if (packageName != null){
                    if (packageName.equals("com.google.android.apps.fireball") || packageName.equals("com.google.android.apps.tachyon")){
                     continue; // Not working at all
                    }


                    if (scheme != null){
                        if (scheme.equals("smsto")){
                            if (packageName != null){
                                if (packageName.equals("com.google.android.talk")){
                                    continue; // Hangouts does not support sms anymore
                                }

                            }
                        }

                        else if (scheme.equals("mailto")){
                            if (packageName.equals("com.udacity.android")){
                                continue; // This is not an email
                            }
                        }
                    }

                }

                HashMap<String, Object> activityItem = new HashMap<>();
                Intent explicitIntent = new Intent(entry.getKey());
                explicitIntent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                String iconKey = info.activityInfo.packageName+info.activityInfo.name;
                if (!icons.containsKey(iconKey)) icons.put(iconKey, info.loadIcon(pm));

                activityItem.put("intent", explicitIntent);
                activityItem.put("name", info.activityInfo.applicationInfo.loadLabel(pm)); //info.loadLabel(pm));
                activityItem.put("label", explicitIntent.getStringExtra("label"));
//                activityItem.put("label", packageName == null ? "no packageName" : packageName);
                activityItem.put("icon", icons.get(iconKey));

                activities.add(activityItem);
            }
        }

        Collections.sort(activities, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> a1, HashMap<String, Object> a2) {
                String name1 = (String) a1.get("name");
                String name2 = (String) a2.get("name");

                if (name1.equals(name2)){
                    String label1 = (String) a1.get("label");
                    String label2 = (String) a2.get("label");

                    return label1.compareTo(label2);
                }
                else return name1.compareTo(name2);
            }
        });

        // Add QR Code item
        HashMap<String, Object> qrCodeItem = new HashMap<>();
        Intent qrCodeIntent = new Intent(this, QRCodeActivity.class);
        qrCodeIntent.putExtra(QRCodeActivity.EXTRA_LINK_KEY, mInvite.getLink());
        qrCodeIntent.putExtra(QRCodeActivity.EXTRA_NAME_KEY, mInfo.contactDisplayName);
        qrCodeIntent.putExtra(EXTRA_NO_CLIP, true);
//        qrCodeItem.put("label", "Show it to someone");
//        qrCodeIntent.putExtra(QRCodeActivity.EXTRA_PHOTO_KEY, )
        qrCodeItem.put("intent", qrCodeIntent);
        qrCodeItem.put("name", getString(R.string.qr_code));

        qrCodeItem.put("icon", ContextCompat.getDrawable(this, R.drawable.ic_qrcode));
        activities.add(0, qrCodeItem);


        final SimpleAdapter adapter = new SimpleAdapter(
                this,
                activities,
                R.layout.actions_list_item,
                new String[]{ "name", "label", "icon"},
                new int[]{ android.R.id.text1, android.R.id.text2, android.R.id.icon }
        );

        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Object value, String stringRepresentation) {
                if (view instanceof ImageView){
                    ((ImageView) view).setImageDrawable((Drawable) value);
                    return true;
                }

                if (view instanceof TextView){
                    if (stringRepresentation.isEmpty()){
                        view.setVisibility(View.GONE);
                    }
                    else {
                        view.setVisibility(View.VISIBLE);
                        ((TextView) view).setText((String) value);
                    }
                    return true;
                }

                return  false;
            }
        });

        mActionsListView.setVisibility(View.VISIBLE);
        mActionsListView.setAdapter(adapter);
        mProgressActionsListView.setVisibility(View.GONE);
    }


    private void setupActionsList(){
        if (mInfo == null) return;
        if (mInvite == null) return;

        mProgressActionsListView.setVisibility(View.VISIBLE);
        mActionsListView.setVisibility(View.GONE);
        fetchRawContacts();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mInfo = parseIntent(intent);
        if (mInvite != null && (mInvite.getSiteId() != mInfo.communityID || mInvite.getContactId() != mInfo.contactID)){
            mInvite = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mInfo == null){
            // TODO: 28.08.2018 Show error message
            setTitle("error");
            updateViews(false);
        }
        else if (mInvite == null){
            setTitle(mInfo.contactDisplayName);
            fetchInviteCode();
        }

        // 1. Check if we have invitation for contact "mContactKey"
        // 1.1 If we have none
        // 1.1.1 Send request to server for creating invitation in appropriate community url (target_url)
        //       and contact key that must become more complex to be unique
        // 1.2 If we have one
        // 1.2.1 Draw invitation preset with uneditable link
        // 1.2.2 Resolve SENDTO intent and make a list of apps to allow user to choose one he wants
        // 1.2.3 On clicking on one of proposed intents (or possibly on onActivityResult()) - send confirmation ot server that current code
        //       was used and change it's status
        // 1.2.4 Remember which intent was used for this code (just to mark it for user)
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SEND_INVITE){
            // Ignore result code

//            if (mInvite.getInvited()){
//                // This means we already done this before
//                return;
//            }
//
//            mInvited=true;
//            invalidateOptionsMenu();
//            markAsInvited();
        }


    }

    private void markAsInvited(){
        executor.execute(new Runnable() {
            @Override
            public void run() {

                AuthState authState = AuthStateManager.getInstance(MessageActivity.this).getCurrent();
                final AuthorizationService authService = new AuthorizationService(MessageActivity.this);

                final String resourceUri = mSite.getResourceUri();
                authState.performActionWithFreshTokens(authService, new AuthState.AuthStateAction() {
                    @Override
                    public void execute(@Nullable String accessToken, @Nullable String idToken, @Nullable AuthorizationException ex) {
                        authService.dispose();

                        if(ex != null){
                            Utils.handleAuthError(MessageActivity.this, findViewById(android.R.id.content), ex);
                            return;
                        }

                        AuthStateManager.getInstance(MessageActivity.this).update();

                        final String ENDPOINT_URI = "invitations/"+mInvite.getId()+"/status";
                        final String requestData = "{\"status\":\"sent\"}";

                        OkHttpClient client = new OkHttpClient();
                        RequestBody body = RequestBody.create(MediaType.parse("application/json"), requestData);

                        Request request = new Request.Builder()
                                .url(resourceUri + ENDPOINT_URI)
                                .addHeader("Authorization", String.format("Bearer %s", accessToken))
                                .post(body)
                                .build();

                        client.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                if (response.isSuccessful()){
                                    AppDatabase db = AppDatabaseFactory.getDatabase(MessageActivity.this);
                                    AppDao dao = db.appDao();
                                    mInvite.setInvited(true);
                                    dao.setInvited(mInvite.getId(), true);

                                    invalidateOptionsMenu();
                                }
                                else {
                                    Log.d("Failed status change", String.format("Status: %d\nMessage: %s", response.code(), response.message()));
                                }

                            }
                        });
                    }
                });


            }
        });

//            executor.execute(new Runnable() {
//                @Override
//                public void run() {
//                    AppDatabase db = AppDatabaseFactory.getDatabase(MessageActivity.this);
//                    AppDao dao = db.appDao();
//                    mInvite.setInvited(true);
//                    dao.setInvited(mInvite.getId(), true);
//
//                    invalidateOptionsMenu();
//                }
//            });
    }

    private void fetchInviteCode() {
        // Try to fetch contact from Invites table
        // If no match
        // Grab contact phone from contentResolver
        // Send phone & name to API and retrieve link for this contact
        // Draw content (hide loader)

        // Show loader
        updateViews(true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                final AppDao dao = AppDatabaseFactory.getDatabase(MessageActivity.this).appDao();

                Invite invite = dao.getInviteByContactAndSite(mInfo.contactID, mInfo.communityID);

                final Site site = dao.getSiteById(mInfo.communityID);
                if (site == null){
                    // TODO: 28.08.2018 Some error here
                    updateViews(false);
                    return;
                }

                mSite = site;

                if (invite != null){ // Found invite in our local DB
                    mInvite = invite;
                    updateViews(false);
                    return;
                }


                String selection = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                String[] selectionArgs = new String[]{ String.valueOf(mInfo.contactID), ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE };

                int NUMBER_IDX = 0;
                int NORMALIZED_NUMBER_IDX = 1;
                int IS_PRIMARY_IDX = 2;

                Cursor cursor = getContentResolver().query(
                        ContactsContract.Data.CONTENT_URI,
                        new String[]{
                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
                                ContactsContract.Data.IS_PRIMARY
                        },
                        selection,
                        selectionArgs,
                        null
                );

                if (cursor == null){
                    // TODO: 28.08.2018 Some error here
                    updateViews(false);
                    return;
                }

                String number = null;
                String normalizedNumber = null;
                boolean primary = false;

                while(!primary && cursor.moveToNext()){
                    primary = cursor.getInt(IS_PRIMARY_IDX)==1;
                    number = cursor.getString(NUMBER_IDX);
                    normalizedNumber = cursor.getString(NORMALIZED_NUMBER_IDX);
                }

                cursor.close();

                final String label = mInfo.contactDisplayName;
                final String identifier = normalizedNumber != null ? normalizedNumber : number;

                if (identifier == null || identifier.isEmpty()){
                    // TODO: 28.08.2018 Some error here
                    updateViews(false);
                    return;
                }

                // Create new invite
                AuthState authState = AuthStateManager.getInstance(MessageActivity.this).getCurrent();
                final AuthorizationService authService = new AuthorizationService(MessageActivity.this);
                authState.performActionWithFreshTokens(authService, new AuthState.AuthStateAction() {
                    @Override
                    public void execute(@Nullable String accessToken, @Nullable String idToken, @Nullable AuthorizationException ex) {
                        authService.dispose();
                        AsyncTaskResult<CreateInviteTask.InviteItem> res = null;

                        if (ex != null){
                            updateViews(false);
                            Log.d("MessageActivity", ex.toJson() + " status" + ex.code);
                            Utils.handleAuthError(MessageActivity.this, findViewById(android.R.id.content), ex);
                            return;
                        }

                        AuthStateManager.getInstance(MessageActivity.this).update();

                        try {
                            CreateInviteTask.ParamsTuple params = new CreateInviteTask.ParamsTuple(
                                    site.getResourceUri(),
                                    accessToken,
                                    identifier,
                                    label
                            );
                            res = new CreateInviteTask().execute(params).get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }

                        if (res == null || res.getResult() == null || res.hasError()){
                            updateViews(false);
                            Log.d("MessageActivity", res != null ? res.getError()+" : "+res.getErrorDescription(): "");
                            Utils.handleAuthError(MessageActivity.this, findViewById(android.R.id.content), res);
                            return;
                        }

                        final Invite newInvite = new Invite(
                                res.getResult().id,
                                res.getResult().label,
                                res.getResult().identifier,
                                res.getResult().link,
                                res.getResult().type,
                                res.getResult().status,
                                res.getResult().usedUid,
                                site.getId(),
                                mInfo.contactID
                        );

                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                dao.insertInvite(newInvite);
                                mInvite = newInvite;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        updateViews(false);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }).start();
    }

    /**
     * Make a use of EXTRAS from intent that have been used to call this activity
     * @param intent Intent passed to call this activity
     */
    private InputRecord parseIntent(Intent intent){
        if (intent == null) return null;


        long communityID = intent.getLongExtra(EXTRA_COMMUNITY_ID, -1);
        long contactID = intent.getLongExtra(EXTRA_CONTACT_ID, -1);
        String contactKey = intent.getStringExtra(EXTRA_CONTACT_KEY);
        String contactName = intent.getStringExtra(EXTRA_CONTACT_NAME);
        String contactDisplayName = intent.getStringExtra(EXTRA_CONTACT_DISPLAY_NAME);
        String communityTitle = intent.getStringExtra(EXTRA_COMMUNITY_TITLE);

        return new InputRecord(communityID, contactID, contactKey, contactName, contactDisplayName, communityTitle);
    }

    static class CreateInviteTask extends AsyncTask<CreateInviteTask.ParamsTuple, Void, AsyncTaskResult<CreateInviteTask.InviteItem>> {
        private static String ENDPOINT_URI = "invitations/add/";
        static class ParamsTuple {
            String resourceUri, accessToken;
            String identifier, label;

            ParamsTuple(String resourceUri, String accessToken, String identifier, String label) {
                this.resourceUri = resourceUri;
                this.accessToken = accessToken;
                this.identifier = identifier;
                this.label = label;
            }
        }

        public static class InviteItem {
            public long id, usedUid;
            public String label, identifier, link, type, status;

            InviteItem(long id, long usedUid, String label, String identifier, String link, String type, String status) {
                this.id = id;
                this.usedUid = usedUid;
                this.label = label;
                this.identifier = identifier;
                this.link = link;
                this.type = type;
                this.status = status;
            }
        }


        @Override
        protected AsyncTaskResult<InviteItem> doInBackground(ParamsTuple... paramsTuples) {
            ParamsTuple params = paramsTuples[0];

            OkHttpClient client = new OkHttpClient();
            JSONObject json = new JSONObject();
            try {
                json.put("identifier", params.identifier);
                if (params.label != null && !params.label.isEmpty())
                    json.put("label", params.label);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(MediaType.parse("application/json"), json.toString());

            Request request = new Request.Builder()
                    .url(params.resourceUri + ENDPOINT_URI)
                    .addHeader("Authorization", String.format("Bearer %s", params.accessToken))
                    .post(body)
                    .build();

            try {
                Response response = client.newCall(request).execute();
                String jsonBody = response.body() != null ? response.body().string() : null;
                JSONObject responseData = new JSONObject(jsonBody);

                /* Fail */
                if (!response.isSuccessful() || responseData.has("error")){
                    return new AsyncTaskResult<>(
                            response.code(),
                            responseData.optString("error", response.message()),
                            responseData.optString("error_description")
                    );
                }
//                if (responseData.has("error")){
//                    return new Result(
//                            response.code(),
//                            responseData.getString("error"),
//                            responseData.has("error_description")
//                                    ? responseData.getString("error_description")
//                                    : null
//                    );
//                }
                /* --- */

                JSONObject item = new JSONObject(jsonBody).optJSONObject("result");
                if (item == null) return new AsyncTaskResult<>("Error", "Incorrect contact identifier");

                return new AsyncTaskResult<>(new InviteItem(
                        item.getLong("id"),
                        item.optLong("used_uid", 0),
                        item.getString("label"),
                        item.getString("identifier"),
                        item.getString("link"),
                        item.getString("type"),
                        item.getString("status")
                ));

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return new AsyncTaskResult<>("Error", null);
        }
    }
}

class InputRecord {
    long contactID, communityID;
    String contactKey, contactName, contactDisplayName, communityTitle;

    InputRecord(long communityID, long contactID, String contactKey, String contactName, String contactDisplayName, String communityTitle) {
        this.communityID = communityID;
        this.communityTitle = communityTitle;
        this.contactID = contactID;
        this.contactKey = contactKey;
        this.contactName = contactName;
        this.contactDisplayName = contactDisplayName;
    }
}

