package com.unet.inviter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class QRCodeActivity extends AppCompatActivity {
    public static String EXTRA_LINK_KEY = "QRCodeActivity.link";
    public static String EXTRA_NAME_KEY = "QRCodeActivity.name";
    public static String EXTRA_PHOTO_KEY = "QRCodeActivity.photoUri";

    // UI
    private ImageView imageView;
    private TextView nameVeiw;

    // Data
    private String link;
    private String name;
    private Uri photoUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back_btn);
            actionBar.setHomeButtonEnabled(true);
        }

        imageView = findViewById(R.id.qrcode_image);
        nameVeiw = findViewById(R.id.name);

        setTitle(R.string.qr_code);

        Intent intent = getIntent();
        if (intent == null) {
            this.finish();
            return;
        }

        link = intent.getStringExtra(EXTRA_LINK_KEY);
        name = intent.getStringExtra(EXTRA_NAME_KEY);
//        photoUri = Uri.parse(intent.getStringExtra(EXTRA_PHOTO_KEY));

        nameVeiw.setText(getString(R.string.qr_code_inv, name));

        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap(link, BarcodeFormat.QR_CODE, 400, 400);
            imageView.setImageBitmap(bitmap);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClose(View v){
        finish();
    }
}
