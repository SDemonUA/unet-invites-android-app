package com.unet.inviter.db;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface AppDao {

    /* ---Sites--- */

    @Query("SELECT * FROM sites WHERE id = :id LIMIT 1")
    Site getSiteById(long id);

    @Query("SELECT id FROM sites")
    long[] getAllSiteIds();

    @Query("SELECT * FROM sites")
    LiveData<List<Site>> getAllSites();

    @Query("SELECT * FROM sites WHERE id IN (:siteIds)")
    LiveData<List<Site>> getSitesByIds(int[] siteIds);

    @Insert(onConflict = OnConflictStrategy.IGNORE) // Do not use REPLACE as it will trigger onDelete=CASCADE for Invites table
    void insertAllSites(Site... sites);

    @Delete
    void deleteSite(Site site);

    @Query("DELETE FROM sites WHERE id IN(:ids)")
    void deleteSitesById(long ...ids);

    @Update
    void updateSite(Site site);

    /* ---Invites--- */

    @Query("SELECT * FROM invites")
    LiveData<List<Invite>> getAllInvites();

    @Query("SELECT * FROM invites WHERE site_id = :siteId")
    LiveData<List<Invite>> getInvitesBySite(long siteId);

    @Query("SELECT * FROM invites WHERE contact_id = :contactId AND site_id = :siteId LIMIT 1")
    Invite getInviteByContactAndSite(long contactId, long siteId);

    @Query("SELECT * FROM invites WHERE site_id = :siteId AND identifier = :identifier")
    LiveData<List<Invite>> getInvitesBySiteAndIdentifier(long siteId, String identifier);

    @Query("UPDATE invites SET invited = :isInvited WHERE id = :id")
    long setInvited(long id, boolean isInvited);

    @Query("UPDATE invites SET invited = :isInvited WHERE contact_id = :contactId AND site_id = :siteId")
    long setInvited(long contactId, long siteId, boolean isInvited);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertInvite(Invite invite);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long[] insertAllInvites(Invite ...invites);

    @Delete
    int deleteInvite(Invite invite);

    @Query("DELETE FROM invites WHERE id IN(:ids)")
    void deleteInvitesById(long ...ids);
}
