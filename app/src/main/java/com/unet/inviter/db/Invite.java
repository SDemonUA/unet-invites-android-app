package com.unet.inviter.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "invites",
        indices = {@Index("site_id"), @Index("identifier"), @Index(value = {"site_id", "contact_id"})},
        foreignKeys = @ForeignKey(entity = Site.class, childColumns = "site_id", parentColumns = "id", onDelete = ForeignKey.CASCADE)
        )
public class Invite {
    @PrimaryKey
    private long id;

    private String label;
    private String identifier;
    private String link;
    private String type;
    private String status;
    private boolean invited = false; /* shows if invitation message was sent to this user (local status) */

    @ColumnInfo(name = "used_uid")
    private long usedUid;

    @ColumnInfo(name = "contact_id")
    private long contactId; /* local contact id to map this invite with user contacts */

    @ColumnInfo(name = "site_id")
    private long siteId;

    public final static String STATUS_NEW = "created";
    public final static String STATUS_NEW_FROM_API = "api-contact";
    public final static String STATUS_PROCESSED = "processed";
    public final static String STATUS_SENT = "sent";
    public final static String STATUS_INVALID = "invalid";
    public final static String STATUS_USED = "used";
    public final static String STATUS_DELETED = "deleted";

    public final static String TYPE_UNET = "unet";
    public final static String TYPE_EMAIL = "email";
    public final static String TYPE_PHONE = "phone";
    public final static String TYPE_FACEBOOK = "facebook";
    public final static String TYPE_LINKEDIN = "linkedin";
    public final static String TYPE_TWITTER = "twitter";
    public final static String TYPE_INSTAGRAM = "instagram";
    public final static String TYPE_VK = "vk";


    public Invite(long id, String label, String identifier, String link, String type, String status, long usedUid, long siteId, long contactId) {
        this.id = id;
        this.label = label;
        this.identifier = identifier;
        this.link = link;
        this.type = type;
        this.status = status;
        this.usedUid = usedUid;
        this.siteId = siteId;
        this.contactId = contactId;
    }

    @Ignore
    public Invite(long id, String label, String identifier, String link, String type, String status, long usedUid, long siteId) {
        this(id, label, identifier, link, type, status, usedUid, siteId, -1);
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public long getUsedUid() {
        return usedUid;
    }
    public void setUsedUid(long usedUid) {
        this.usedUid = usedUid;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getIdentifier() {
        return identifier;
    }
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public long getContactId() {
        return contactId;
    }
    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public long getSiteId() {
        return siteId;
    }
    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public boolean getInvited(){ return invited; }
    public void setInvited(boolean invited){ this.invited = invited; }
}

