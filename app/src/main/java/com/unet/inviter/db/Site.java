package com.unet.inviter.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "sites")
public class Site {
    @PrimaryKey
    private long id;

    private String title;
    private String url;
    @ColumnInfo(name = "logo_url")
    private String logoUrl;
    @ColumnInfo(name = "resource_uri")
    private String resourceUri;

    public Site(long id, String title, String url, String logoUrl, String resourceUri) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.logoUrl = logoUrl;
        this.resourceUri = resourceUri;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }
}
