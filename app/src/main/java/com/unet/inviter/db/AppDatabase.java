package com.unet.inviter.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Site.class, Invite.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract AppDao appDao();
}
