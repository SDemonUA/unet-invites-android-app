package com.unet.inviter.db;

import androidx.room.Room;
import android.content.Context;
import android.util.Log;

import com.unet.inviter.AuthStateManager;

import net.openid.appauth.TokenResponse;

import java.util.WeakHashMap;


public class AppDatabaseFactory {
    private static final WeakHashMap<String, AppDatabase> mInstances = new WeakHashMap<>();
    public static AppDatabase getDatabase(Context context){
        return getDatabase(context, getUserDBName(context));
    }
    public static AppDatabase getDatabase(Context context, String dbName) {
        if (!mInstances.containsKey(dbName)){
            mInstances.put(dbName, Room.databaseBuilder(context, AppDatabase.class, dbName).build());
        }

        return mInstances.get(dbName);
    }
    public static String getUserDBName(Context context) {
        TokenResponse lastTokenResponse = AuthStateManager.getInstance(context).getCurrent().getLastTokenResponse();
        String user_id = "";
        if (lastTokenResponse == null) Log.w("AppDatabase", "Attempt to achieve dbName for not authenticated user");
        else user_id = lastTokenResponse.additionalParameters.get("user_id");

        return "db_"+user_id;
    }
}
