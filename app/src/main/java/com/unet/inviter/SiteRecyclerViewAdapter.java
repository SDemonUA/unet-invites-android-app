package com.unet.inviter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.unet.inviter.SitesListFragment.OnListFragmentInteractionListener;
import com.unet.inviter.db.Site;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

/**
 * {@link RecyclerView.Adapter} that can display a {@link com.unet.inviter.db.Site} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class SiteRecyclerViewAdapter extends RecyclerView.Adapter<SiteRecyclerViewAdapter.ViewHolder> {

    public interface OnListItemInteractionListener {
        void onClick(Site item);
    }

    private final LiveData<List<Site>> mValues;
    private final OnListItemInteractionListener mListener;
    private long mSelectedId = -1; // -1 no selection

    private Observer<List<Site>> liveDataObserver = new Observer<List<Site>>() {
        @Override
        public void onChanged(@Nullable List<Site> sites) {
            notifyDataSetChanged();
        }
    };

    public SiteRecyclerViewAdapter(LiveData<List<Site>> items, OnListItemInteractionListener listener) {
        mValues = items;
        mListener = listener;

        mValues.observeForever(liveDataObserver);
    }

    public SiteRecyclerViewAdapter(LiveData<List<Site>> items, OnListItemInteractionListener listener, long selectedId){
        this(items, listener);
        mSelectedId = selectedId;
    }

    public void setSelectedId(long newSelectedId){
        if (newSelectedId == mSelectedId) return;

        int[] idxs = null;
        if (mValues.getValue() != null){
            idxs = new int[2];
            for (int i=0; i<mValues.getValue().size(); i++){
                if (mValues.getValue().get(i).getId() == mSelectedId) idxs[0] = i;
                if (mValues.getValue().get(i).getId() == newSelectedId) idxs[1] = i;
            }
        }

        mSelectedId = newSelectedId;

        if (idxs != null){
            notifyItemChanged(idxs[0]);
            notifyItemChanged(idxs[1]);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.site_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.getValue().get(position);
        holder.mTitleView.setText(mValues.getValue().get(position).getTitle());

        Picasso.get()
                .load(mValues.getValue().get(position).getLogoUrl())
                .into(holder.mIconView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    setSelectedId(holder.mItem.getId());
                    mListener.onClick(holder.mItem);
                }
            }
        });

        // Set item selected state
        holder.mView.setActivated(holder.mItem.getId() == mSelectedId);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        mValues.removeObserver(liveDataObserver);
        liveDataObserver = null;
    }

    @Override
    public int getItemCount() {
        return mValues.getValue() == null ? 0 : mValues.getValue().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final RoundedImageView mIconView;
        final TextView mTitleView;
        public Site mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mIconView = view.findViewById(R.id.icon);
            mTitleView = view.findViewById(R.id.title);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitleView.getText() + "'";
        }
    }
}
