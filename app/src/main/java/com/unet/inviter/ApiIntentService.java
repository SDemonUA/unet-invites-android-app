package com.unet.inviter;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.unet.inviter.db.AppDao;
import com.unet.inviter.db.AppDatabase;
import com.unet.inviter.db.AppDatabaseFactory;
import com.unet.inviter.db.Invite;
import com.unet.inviter.services.ApiServiceContract;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationService;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * helper methods.
 */
public class ApiIntentService extends IntentService {
    private static final String ACTION_MARK_AS_INVITED = "com.unet.inviter.action.MARK_AS_INVITED";

    private static final String EXTRA_SITE_RESOURCE_URI = "com.unet.inviter.extra.SITE_RESOURCE_URI";
    private static final String EXTRA_INVITE_ID = "com.unet.inviter.extra.INVITE_ID";

    private ExecutorService executor = Executors.newSingleThreadExecutor();

    public ApiIntentService() {
        super("ApiIntentService");
    }

    /**
     * Starts this service to perform action MarkAsInvited with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionMarkAsInvited(Context context, String siteResourceUri, long inviteId) {
        Intent intent = new Intent(context, ApiIntentService.class);
        intent.setAction(ACTION_MARK_AS_INVITED);
        intent.putExtra(EXTRA_SITE_RESOURCE_URI, siteResourceUri);
        intent.putExtra(EXTRA_INVITE_ID, inviteId);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_MARK_AS_INVITED.equals(action)) {
                final String siteResourceUri = intent.getStringExtra(EXTRA_SITE_RESOURCE_URI);
                final long inviteId = intent.getLongExtra(EXTRA_INVITE_ID, -1);
                handleActionMarkAsInvited(siteResourceUri, inviteId);
            }
        }
    }

    /**
     * Handle action MarkAsInvited in the provided background thread with the provided
     * parameters.
     */
    private void handleActionMarkAsInvited(final String siteResourceUri, final long inviteId) {

        AuthState authState = AuthStateManager.getInstance(getApplicationContext()).getCurrent();
        final AuthorizationService authService = new AuthorizationService(getApplicationContext());

        authState.performActionWithFreshTokens(authService, new AuthState.AuthStateAction() {
            @Override
            public void execute(@Nullable String accessToken, @Nullable String idToken, @Nullable AuthorizationException ex) {
                authService.dispose();

                if(ex != null){
                    Intent intent = new Intent(ApiServiceContract.BROADCAST_INVITE_STATUS)
                            .putExtra(ApiServiceContract.EXTRA_STATUS, ApiServiceContract.Status.ERROR.name())
                            .putExtra(ApiServiceContract.EXTRA_ERROR, ApiServiceContract.Error.AUTH_ERROR.name());
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    return;
                }

                // Update access & refresh tokens on disk
                AuthStateManager.getInstance(getApplicationContext()).update();

                final String ENDPOINT_URI = "invitations/"+inviteId+"/status";
                final String requestData = "{\"status\":\"sent\"}";

                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(MediaType.parse("application/json"), requestData);

                Request request = new Request.Builder()
                        .url(siteResourceUri + ENDPOINT_URI)
                        .addHeader("Authorization", String.format("Bearer %s", accessToken))
                        .post(body)
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    if (!response.isSuccessful()){
                        Intent intent = new Intent(ApiServiceContract.BROADCAST_INVITE_STATUS)
                                .putExtra(ApiServiceContract.EXTRA_STATUS, ApiServiceContract.Status.ERROR.name())
                                .putExtra(ApiServiceContract.EXTRA_ERROR, response.code() == 500
                                        ? ApiServiceContract.Error.SERVER_ERROR.name()
                                        : ApiServiceContract.Error.REQUEST_ERROR.name()
                                )
                                .putExtra(ApiServiceContract.EXTRA_ERROR_MESSAGE, response.body().string());

                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                        Log.d("Failed status change",
                                String.format("Status: %d\nMessage: %s", response.code(), response.message()));
                        return;
                    }

                    AppDatabase db = AppDatabaseFactory.getDatabase(getApplicationContext());
                    AppDao dao = db.appDao();
                    dao.setInvited(inviteId, true);

                    Intent intent = new Intent(ApiServiceContract.BROADCAST_INVITE_STATUS)
                            .putExtra(ApiServiceContract.EXTRA_STATUS, ApiServiceContract.Status.DONE.name())
                            .putExtra(ApiServiceContract.EXTRA_DATA, Invite.STATUS_SENT);

                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                } catch (IOException e) {
                    Intent intent = new Intent(ApiServiceContract.BROADCAST_INVITE_STATUS)
                            .putExtra(ApiServiceContract.EXTRA_STATUS, ApiServiceContract.Status.ERROR.name())
                            .putExtra(ApiServiceContract.EXTRA_ERROR, ApiServiceContract.Error.TASK_ERROR.name())
                            .putExtra(ApiServiceContract.EXTRA_ERROR_MESSAGE, e.getMessage());

                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    e.printStackTrace();
                }
            }
        });
    }
}
